Uruchomienie projektu:

$ docker-compose build && docker compose up -d 

wejście do kontenera 

$ docker exec -it -u www-data example-api_php_1 bash 

$ composer install 

$ mkdir -p config/jwt

Generownie kluczy

$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096

$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

podmieniamy w .env (który jest tutaj załączony, tak aby mieć od razu pełen config)  

JWT_PASSPHRASE=test

budujemy baze, wykonujac doctrine:schema:create oraz wykonując migrację, następnie ładujemy fixtury (jest tam użytkowik z rolą ROLE_USER)

Pobieranie tokena JWT bearer (podmieniamy emial usera z fixtur) : 


```
curl --location --request POST 'http://localhost/index.php/authentication_token' \
--header 'Content-Type: application/json' \
--data-raw '{"email":"email","password":"haslo123!"}'

```

http://localhost/index.php/api/docs Tutaj widzimy endpointy. Możemy testować przez swaggera po kliknięciu Autorize i wpisując BEARER _nasz_token_ 

Aplikacja zakłada zagłosowanie na pozytywny i negatynwy głos na film.

Co nie zostało zrobione z braku czasu a co bym zrobił: 

* Nie dodany dekorator Swaggera odnośnie autoryzacji. (Można by było token pobrać już na stronie docs i widzieć co trzeba podać w autoryzcji) (https://api-platform.com/docs/core/jwt/#documenting-the-authentication-mechanism-with-swaggeropen-api)
* Nie wydzielona logika biznesowa, brak części domenowej która była by otestowana. (miałem to zrobić, stąd też namespace App przeniesiony do src/App) logika była by zawarat w innym Module. 
* Brak relacji do pobrania głosów policzonych dla filmu (głosy pozytywne/negarynwe) 
* Brak lepszej obsługi błędów 
* Logika Validatora powinna być weksraktowana do osobnej klasy która powinna być otestowana (bo to ona jest cześcią domenową) a następnie MovieVoteValidator powinien ją tylko wstrzykiwać
* Wszystko zostało zrobione a następnie scommitowane na repo, bez podziału na commity


Te rzeczy nie zostały wykonane z racji braku czasu którego więcej, nie mogę na to zadanie poświęcić. 
Jeżeli chcesz zapoznać się, z moim podejściem do kodu który jest framework agnostic i skupia się na domenie, zapraszam do mojego drugiego repozytoriu:

https://bitbucket.org/przemek_stawinoga/example/src/master/


