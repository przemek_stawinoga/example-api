<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\MovieVote;
use App\Repository\MovieRepository;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MovieVoteInputDataTransformer implements DataTransformerInterface
{
    private MovieRepository $movieRepository;
    private TokenStorageInterface $tokenStorage;
    private UserRepository     $userRepository;
    private ValidatorInterface $validator;

    public function __construct(
        ValidatorInterface $validator,
        MovieRepository $movieRepository,
        UserRepository $userRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->validator       = $validator;
        $this->movieRepository = $movieRepository;
        $this->userRepository  = $userRepository;
        $this->tokenStorage    = $tokenStorage;
    }

    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object, $context);
        $user  = $this->tokenStorage->getToken()->getUser();
        $user  = $this->userRepository->findOneBy(['email' => $user->getUsername()]);
        $movie = $this->movieRepository->find($object->getMovieId());

        $vote = new MovieVote();
        $vote->setMovie($movie);
        $vote->setUser($user);
        $vote->setVote($object->getVote());

        return $vote;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return MovieVote::class === $to;
    }
}
