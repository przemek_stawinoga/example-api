<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\MovieVoteOutput;
use App\Entity\MovieVote;

class MovieVoteOutputDataTransformer implements DataTransformerInterface
{
    public function transform($object, string $to, array $context = [])
    {
        $vote = new MovieVoteOutput();

        $vote->id      = $object->getId();
        $vote->vote    = $object->getVote();
        $vote->movieId = $object->getMovie()->getId();
        $vote->userId  = $object->getUser()->getId();

        return $vote;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return MovieVoteOutput::class === $to && $data instanceof MovieVote;
    }
}
