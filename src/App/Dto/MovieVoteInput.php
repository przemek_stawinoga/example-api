<?php

namespace App\Dto;

use Symfony\Component\Serializer\Annotation\Groups;

class MovieVoteInput
{
    /**
     * @Groups({"write"})
     */
    public int  $movieId;

    /**
     * @Groups({"write"})
     */
    public bool $vote;

    public function getMovieId(): int
    {
        return $this->movieId;
    }

    public function getVote(): bool
    {
        return $this->vote;
    }
}
