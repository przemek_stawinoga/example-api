<?php

namespace App\Dto;

use Symfony\Component\Serializer\Annotation\Groups;

class MovieVoteOutput
{
    /**
     * @Groups({"read"})
     */
    public int  $id;

    /**
     * @Groups({"read"})
     */
    public int  $movieId;
    /**
     * @Groups({"read"})
     */
    public int  $userId;
    /**
     * @Groups({"read"})
     */
    public bool $vote;
}
