<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MovieVoteRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Dto\MovieVoteInput;
use App\Dto\MovieVoteOutput;

/**
 * Secured resource.
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     input=MovieVoteInput::class,
 *     output=MovieVoteOutput::class,
 *     collectionOperations={
 *         "post"={"security"="is_granted('ROLE_USER')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_USER')"}
 *     }
 * )
 * @ORM\Table(name="movie_vote",
 *    uniqueConstraints={
 *        @ORM\UniqueConstraint(name="movie_unique",
 *            columns={"user_id", "movie_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass=MovieVoteRepository::class)
 */
class MovieVote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Movie::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $movie;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vote;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    public function setMovie(?Movie $movie): self
    {
        $this->movie = $movie;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getVote(): bool
    {
        return $this->vote;
    }

    public function setVote(bool $vote): void
    {
        $this->vote = $vote;
    }
}
