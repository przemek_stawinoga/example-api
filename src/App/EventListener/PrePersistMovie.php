<?php

namespace App\EventListener;

use App\Entity\Movie;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;

class PrePersistMovie
{
    private UsageTrackingTokenStorage $tokenStorage;

    public function __construct(UsageTrackingTokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Movie) {
            return;
        }

        try {
            $user = $this->tokenStorage->getToken()->getUser();

            if (null !== $user) {
                $entity->setOwner($user);
            }
        } catch (\Exception $e) {
            throw new \Exception('Cannot create user when user is not logged');
        }
    }
}
