<?php

namespace App\Repository;

use App\Entity\MovieVote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MovieVote|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovieVote|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovieVote[]    findAll()
 * @method MovieVote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieVoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovieVote::class);
    }
}
