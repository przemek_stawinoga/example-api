<?php

namespace App\Validator;

use ApiPlatform\Core\Validator\Exception\ValidationException;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\MovieVoteInput;
use App\Repository\MovieRepository;
use App\Repository\MovieVoteRepository;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;

class MovieVoteValidator implements ValidatorInterface
{
    private MovieRepository           $movieRepository;
    private MovieVoteRepository       $movieVoteRepository;
    private UsageTrackingTokenStorage $tokenStorage;
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(
        MovieRepository $movieRepository,
        MovieVoteRepository $movieVoteRepository,
        UserRepository $userRepository,
        UsageTrackingTokenStorage $tokenStorage
    ) {
        $this->movieRepository     = $movieRepository;
        $this->movieVoteRepository = $movieVoteRepository;
        $this->userRepository      = $userRepository;
        $this->tokenStorage        = $tokenStorage;
    }

    public function validate($data, array $context = [])
    {
        if (!$data instanceof MovieVoteInput) {
            throw new ValidationException("Invalid type of data argument");
        }

        $movie = $this->movieRepository->find($data->getMovieId());

        if (empty($movie)) {
            throw new ValidationException("Invalid movieId");
        }

        $loggedUser = $this->tokenStorage->getToken()->getUser();

        if (empty($loggedUser)) {
            throw new ValidationException("Invalid user, check your authorization");
        }

        $user = $this->userRepository->findOneBy(['email' => $loggedUser->getUsername()]);

        if (empty($user)) {
            throw new ValidationException("User not found");
        }

        $vote = $this->movieVoteRepository->findOneBy(['user' => $user, 'movie' => $movie]);

        if (!empty($vote)) {
            throw new ValidationException("The user has already voted for the movie");
        }
    }
}
